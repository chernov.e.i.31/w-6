using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.DataAccess.DBaseContext;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        :   ICustomerRepository
    {
        DbContextOptions<DataContext> _options;
        public CustomerRepository(DbContextOptions<DataContext> options)
        {
            _options = options;
        }

        public void InitDB()
        {
            using (DataContext dbContext = new DataContext(_options))
            {
                dbContext.Database.EnsureDeleted();
                dbContext.Database.EnsureCreated();
            }
        }

        public void AddCustomer(Customer customer)
        {
            using (DataContext dbContext = new DataContext(_options))
            {
                try
                {
                    dbContext.Customers.Add(customer);
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            //Add customer to data source   
        }
        public int AccountCount()
        {
            using (DataContext dbContext = new DataContext(_options))
            {
                return dbContext.Customers.Count();
            }
        }
    }
}