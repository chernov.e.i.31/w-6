﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Diagnostics;
using System.IO;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private const string _generateProcessFileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private const string _generateProcessDirectory = @"D:\project\OTUS\w6\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\net6.0";

        private static string _fileName = "customers111";
        private static int _countRecord = 10000;
        private static string _dataFilePath;


        static void Main()
        {
            Console.WriteLine("Введите тип запуска: 1-процесс, 2-метод");

            int startType;
            while (true)
            {
                var value = Console.ReadLine();
                if (int.TryParse(value, out startType))
                {
                    if (startType == 1 || startType == 2)
                    {
                        break;
                    }
                }
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if (startType == 1)
            {
                _dataFilePath = Path.Combine(_generateProcessDirectory, _fileName + ".xml");
                GenerateCustomersDataProcess(_fileName).WaitForExit();
            }
            else
            {
                _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _fileName + ".xml");
                GenerateCustomersDataFile();
            }

            var loader = new FakeDataLoader();

            loader.LoadData(_dataFilePath);
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, _countRecord);
            xmlGenerator.Generate();
        }

        static Process GenerateCustomersDataProcess(string fileName)
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { fileName, _countRecord.ToString() },
                FileName = Path.Combine(_generateProcessDirectory, _generateProcessFileName),
            };

            var process = Process.Start(startInfo);

            return process;

        }
    }
}