using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.DBaseContext;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        readonly List<Thread> _queueThreads = new List<Thread>();
        private static int _countTry = 10;

        DbContextOptionsBuilder<DataContext> optionsBuilder;

        public void LoadData(string path)
        {
            Console.WriteLine("������� ��� ��: 1-sqlite, 2-pg");

            int startType;
            while (true)
            {
                var value = Console.ReadLine();
                if (int.TryParse(value, out startType))
                {
                    if (startType == 1 || startType == 2)
                    {
                        break;
                    }
                }
            }
            Console.WriteLine("Loading data...");

            if (startType == 1)
            {
                #region sqllite
                var connectionstring = "Filename=DBCustomers.sqlite";
                optionsBuilder = new DbContextOptionsBuilder<DataContext>();
                optionsBuilder.UseSqlite(connectionstring);
                #endregion
            }
            else
            {
                #region PG
                var connectionstring = "Host=localhost;port=5432;Username=postgres;Password=12345;Database=DBCustomers";
                optionsBuilder = new DbContextOptionsBuilder<DataContext>();
                optionsBuilder.UseNpgsql(connectionstring);
                #endregion
            }

            var repo = new CustomerRepository(optionsBuilder.Options);
            repo.InitDB();

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            IDataParser<List<Customer>> parser = new XmlParser(path);
            var data = parser.Parse();

            var process = Environment.ProcessorCount;
            var chunkSize = data.Count / process;
            var chunkedList = data.Chunk(chunkSize);
            foreach (var chunk in chunkedList)
            {
                _queueThreads.Add(StartThread(chunk, true));
            }
            _queueThreads.ForEach(x => x.Join());

            Console.WriteLine($"������� �������: {repo.AccountCount()}");
            Console.WriteLine("Loaded data...");
            stopWatch.Stop();
            Console.WriteLine($"����� ������ {stopWatch.Elapsed.ToString(@"m\:ss\.fff")}");
        }

        private Thread StartThread(IEnumerable<Customer> customers, bool isBackground = false)
        {
            var thread = new Thread(() => SendData(customers)) { IsBackground = isBackground };
            thread.Start();
            return thread;
        }

        private void SendData(IEnumerable<Customer> customers)
        {
            var repo = new CustomerRepository(optionsBuilder.Options);
            foreach (var cust in customers)
            {
                var tryNumber = 0;
                var isError = false;
                do
                {
                    tryNumber++;
                    try
                    {
                        repo.AddCustomer(cust);
                        isError = false;
                    }
                    catch
                    {
                        Console.WriteLine("����������");
                        Thread.Sleep(100);
                        isError = true;
                    }
                } while (isError && tryNumber < _countTry);
            }
        }
    }
}